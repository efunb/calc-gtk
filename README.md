# `calc-gtk`

[![pipeline status](https://gitlab.com/efunb/calc-gtk/badges/master/pipeline.svg)](https://gitlab.com/efunb/calc-gtk/commits/master)


## Help

If you run into any issues or need help with using `calc-gtk` in your project please email [incoming+efunb-calc-gtk-12593244-issue-@incoming.gitlab.com](incoming+efunb-calc-gtk-12593244-issue-@incoming.gitlab.com)


## Install
### Compile yourself

```sh
git clone https://gitlab.com/efunb/calc-gtk.git
cd calc-gtk/
stack install .
```

### Download

[Download for Linux](https://gitlab.com/efunb/calc-gtk/-/jobs/artifacts/master/raw/files/calc-gtk-exe?job=linux-optimized)

## **Warning**

**If you are viewing this from GitHub then this is a read only copy. Please contribute to the GitLab copy [here](https://gitlab.com/efunb/calc-gtk).**